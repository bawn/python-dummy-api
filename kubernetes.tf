resource "kubernetes_deployment" "python-dummy-api" {
  metadata {
    name = "python-dummy-api"
    labels = {
      App = "python-dummy-api"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        App = "python-dummy-api"
      }
    }
    template {
      metadata {
        labels = {
          App = "python-dummy-api"
        }
      }
      spec {
        container {
          image = "xpose/python-rest"
          name  = "python-dummy-api"

          resources {
            limits {
              memory = "300Mi"
            }
            requests {
              memory = "300Mi"
            }
          }

          port {
            container_port = 5000
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "python-dummy-svc" {
  metadata {
    name = "python-dummy-svc"
  }
  spec {
    selector = {
      App = "python-dummy-api"
    }
    port {
      port        = 80
      target_port = 5000
    }

    type = "LoadBalancer"
  }
}

resource "kubernetes_ingress" "python-dummy-ingress" {
  metadata {
    name = "python-dummy-ingress"
  }
  spec {
    backend {
      service_name = "python-dummy-svc"
      service_port = "80"
    }

    rule {
      http {
        path {
          backend {
            service_name = "python-dummy-svc"
            service_port = "80"
          }

          path = "/*"
        }
      }
    }
  }
}

resource "kubernetes_limit_range" "python-dummy-limit" {
  metadata {
    name = "python-dummy-limit"
  }

  spec {
    limit {
      type = "Pod"
      max = {
        memory = "350Mi"
        ephemeral-storage = "45Gi"
      }

      min = {
        memory = "300Mi"
        ephemeral-storage = "30Gi"
      }
    }
  }
}
