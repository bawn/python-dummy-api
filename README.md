# Python API Docker Test

steps to run the sample rest service on docker -

1. Clone the Repository - git clone https://gitlab.com/hek23/python-dummy-api

2. Move to the directory - cd python-rest-api-docker

3. Build the docker image - docker build -t python-rest .

4. Create and run a container - docker run -d -p 5000:5000 python-rest

5. Navigate to http://0.0.0.0:5000/ to get hello world


The solution is to use terraform in GKE consisting of the following:

1. Test the operation of k8s locally, the configured files are located in the k8s folder of the repository
2. Generate and publish dockerfile in docker hub for image utilization
3. Create and configure files for terraform and add .gitignore
4. Gitlab CI is configured in operations to connect with gke
5. Modifies the .gitlab-ci.yml file to use terraform and apply the configuration

